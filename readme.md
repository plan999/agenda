#### Plan 9
 
* PHP releases API
  * [Adroit Framework](https://gitlab.com/adroit4/framework/-/tree/main/)
  * [OpenApi](https://oai.github.io/Documentation/start-here.html) support
  * [Тестирование ПО: Postman для тестирования API](https://stepik.org/course/120679/syllabus)
  * [Web Scrapping with PHP](https://www.phparch.com/books/web-scraping-with-php-2nd-edition/)
* Verisim
  * TALL Stack: TailwindCSS, AlpineJS, Livewire, Laravel
  * [Web Apps for Bots](https://core.telegram.org/bots/webapps)
  * [Verisim Git](https://gitlab.com/startup14/telegop)
* Java
  * [Java. Базовый курс](https://stepik.org/course/187)
  * [Java. Functional Programming](https://stepik.org/course/91497/syllabus)
* Books
  * The Art of Modern PHP 8 (2021)
  * Web Scraping with PHP (2010)
  * Functional programming with PHP (2016)
  * The Practice of Programming (1999)
* Behind
  * [Разработка расширений для PHP на PHP](https://habr.com/ru/company/oleg-bunin/blog/577658/)
